import Line from '../models/line.model';

export interface WaitingTimesProvider {
  getByLocationId(locationId: string): Promise<Line[]>;
}
