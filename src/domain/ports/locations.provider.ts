import Location from '../models/location.model';

export interface LocationsProvider {
  getAll(): Promise<Location[]>;
}
