import LineColorDto from '../models/line-color.dto';

export interface LinesColorRepository {
  findAll(): Promise<LineColorDto[]>;
}
