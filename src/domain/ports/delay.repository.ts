import NotifyDelayDto from '../models/notify-delay.dto';

export interface DelayRepository {
  notifyDelay(notifyDelayDto: NotifyDelayDto): Promise<NotifyDelayDto>;
}
