export default class LineColorDto {
  line: string;
  color: string;
  textColor: string;
}
