import DirectionValue from './direction-value.model';

export default class Directions {
  forward: DirectionValue[];
  backward: DirectionValue[];
}
