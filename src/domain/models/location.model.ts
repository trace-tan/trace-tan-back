import Line from './line.model';

export default class Location {
  locationId: string;
  label: string;
  lines: Pick<Line, 'line'>[];
}
