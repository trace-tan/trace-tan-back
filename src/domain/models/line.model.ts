import Directions from './directions.model';
import { LineType } from './line-type.enum';

export default class Line {
  line: string;
  lineType: LineType;
  directions: Directions;
}
