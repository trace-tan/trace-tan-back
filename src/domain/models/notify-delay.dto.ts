export default class NotifyDelayDto {
  stopId: string;
  terminusDirection: string;
  departureTime: Date;
  delay: number;
  line: string;
}
