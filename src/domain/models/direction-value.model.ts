import WaitingTime from './waiting-time.model';

export default class DirectionValue {
  terminus: string;
  stopId: string;
  times: WaitingTime[];
}
