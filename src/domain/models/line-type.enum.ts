export enum LineType {
  TRAM = 'tramway',
  BUSWAY = 'busway',
  BUS = 'bus',
  NAVIBUS = 'navibus',
  UNKNOWN = 'unknown',
}
