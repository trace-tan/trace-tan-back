import { HttpModule, Module } from '@nestjs/common';
import { DatabaseModule } from '../infrastructure/right-adapters/persistence/database.module';
import { DelayRepositoryPostgres } from '../infrastructure/right-adapters/persistence/repository/delay.repository.postgres';
import { LinesColorRepositoryPostgres } from '../infrastructure/right-adapters/persistence/repository/lines-color.repository.postgres';
import { LocationsProviderTanApi } from '../infrastructure/right-adapters/tan-api/locations.provider.tan-api';
import { WaitingTimesProviderTanApi } from '../infrastructure/right-adapters/tan-api/waiting-times.provider.tan-api';
import {
  DELAY_REPOSITORY,
  LINES_COLOR_REPOSITORY,
  LOCATIONS_PROVIDER,
  WAITING_TIMES_PROVIDER,
} from './constant';
import { GetLinesColorUseCase } from './use-cases/get-lines-color';
import { GetWaitingTimesOfLocationUseCase } from './use-cases/get-waiting-times-of-location.usecase';
import { ListLocationsUseCase } from './use-cases/list-locations.usecase';
import { NotifyDelayUseCase } from './use-cases/notify-delay';

@Module({
  imports: [HttpModule, DatabaseModule],
  providers: [
    GetWaitingTimesOfLocationUseCase,
    ListLocationsUseCase,
    NotifyDelayUseCase,
    GetLinesColorUseCase,
    {
      provide: WAITING_TIMES_PROVIDER,
      useClass: WaitingTimesProviderTanApi,
    },
    {
      provide: LOCATIONS_PROVIDER,
      useClass: LocationsProviderTanApi,
    },
    {
      provide: DELAY_REPOSITORY,
      useExisting: DelayRepositoryPostgres,
    },
    {
      provide: LINES_COLOR_REPOSITORY,
      useExisting: LinesColorRepositoryPostgres,
    },
  ],
  exports: [
    GetWaitingTimesOfLocationUseCase,
    ListLocationsUseCase,
    NotifyDelayUseCase,
    GetLinesColorUseCase,
  ],
})
export class ApplicationModule {}
