import { Inject, Injectable } from '@nestjs/common';
import NotifyDelayDto from '../../domain/models/notify-delay.dto';
import { DelayRepository } from '../../domain/ports/delay.repository';
import { DELAY_REPOSITORY } from '../constant';

@Injectable()
export class NotifyDelayUseCase {
  constructor(
    @Inject(DELAY_REPOSITORY)
    private readonly delayRepository: DelayRepository,
  ) {}

  public handler(notifyDelay: NotifyDelayDto): Promise<NotifyDelayDto> {
    return this.delayRepository.notifyDelay(notifyDelay);
  }
}
