import { Inject, Injectable } from '@nestjs/common';
import Location from '../../domain/models/location.model';
import { LocationsProvider } from '../../domain/ports/locations.provider';
import { LOCATIONS_PROVIDER } from '../constant';

@Injectable()
export class ListLocationsUseCase {
  constructor(
    @Inject(LOCATIONS_PROVIDER)
    private readonly locationsProvider: LocationsProvider,
  ) {}

  public handler(): Promise<Location[]> {
    return this.locationsProvider.getAll();
  }
}
