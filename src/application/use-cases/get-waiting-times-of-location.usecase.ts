import { Inject, Injectable } from '@nestjs/common';
import Line from '../../domain/models/line.model';
import { WaitingTimesProvider } from '../../domain/ports/waiting-times.provider';
import { WAITING_TIMES_PROVIDER } from '../constant';

@Injectable()
export class GetWaitingTimesOfLocationUseCase {
  constructor(
    @Inject(WAITING_TIMES_PROVIDER)
    private readonly waitingTimesProvider: WaitingTimesProvider,
  ) {}

  public handler(locationId: string): Promise<Line[]> {
    return this.waitingTimesProvider.getByLocationId(locationId);
  }
}
