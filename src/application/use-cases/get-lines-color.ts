import { Inject, Injectable } from '@nestjs/common';
import LineColorDto from '../../domain/models/line-color.dto';
import { LinesColorRepository } from '../../domain/ports/lines-color.repository';
import { LINES_COLOR_REPOSITORY } from '../constant';

@Injectable()
export class GetLinesColorUseCase {
  constructor(
    @Inject(LINES_COLOR_REPOSITORY)
    private readonly linesColorRepository: LinesColorRepository,
  ) {}

  public handler(): Promise<LineColorDto[]> {
    return this.linesColorRepository.findAll();
  }
}
