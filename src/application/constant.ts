export const WAITING_TIMES_PROVIDER = 'WaitingTimesProvider';
export const LOCATIONS_PROVIDER = 'LocationsProvider';
export const DELAY_REPOSITORY = 'DelayRepository';
export const LINES_COLOR_REPOSITORY = 'LinesColorRepository';
