import { HttpService, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import Line from '../../../domain/models/line.model';
import { WaitingTimesProvider } from '../../../domain/ports/waiting-times.provider';
import LinesMapper from './mapper/lines.mapper';

@Injectable()
export class WaitingTimesProviderTanApi implements WaitingTimesProvider {
  constructor(
    private readonly httpService: HttpService,
    private readonly configService: ConfigService,
  ) {}
  getByLocationId(locationId: string): Promise<Line[]> {
    const url = this.configService.get('TAN_API_URL');
    return this.httpService
      .get(`${url}tempsattente.json/${locationId}`)
      .toPromise()
      .then((value) => {
        return LinesMapper.toDomains(value.data);
      });
  }
}
