export interface LineEntity {
  readonly numLigne: string;
  readonly typeLigne: number;
}
