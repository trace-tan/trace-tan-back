import { LineEntity } from './line.entity';

export interface LocationEntity {
  readonly codeLieu: string;
  readonly libelle: string;
  readonly ligne: Pick<LineEntity, 'numLigne'>[];
}
