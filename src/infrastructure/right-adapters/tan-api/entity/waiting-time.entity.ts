import { LineEntity } from './line.entity';
import { StopEntity } from './stop.entity';

export interface WaitingTimeEntity {
  readonly sens: number;
  readonly terminus: string;
  readonly temps: string;
  readonly ligne: LineEntity;
  readonly arret: StopEntity;
}
