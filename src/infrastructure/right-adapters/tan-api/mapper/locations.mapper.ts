import Line from '../../../../domain/models/line.model';
import Location from '../../../../domain/models/location.model';
import { LocationEntity } from '../entity/location.entity';

export default class LocationMapper {
  public static toDomains(locationEntities: LocationEntity[]): Location[] {
    if (!locationEntities || locationEntities.length == 0) {
      return [];
    }
    const locations: Location[] = [];
    locationEntities.forEach((locationEntity) => {
      const lines: Pick<Line, 'line'>[] = [];
      locationEntity.ligne.forEach((ligne) => {
        lines.push({ line: ligne.numLigne });
      });
      locations.push({
        locationId: locationEntity.codeLieu,
        label: locationEntity.libelle,
        lines,
      });
    });
    return locations;
  }
}
