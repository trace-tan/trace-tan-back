import DirectionValue from '../../../../domain/models/direction-value.model';
import { Direction } from '../../../../domain/models/direction.enum';
import { LineType } from '../../../../domain/models/line-type.enum';
import Line from '../../../../domain/models/line.model';
import { WaitingTimeEntity } from '../entity/waiting-time.entity';

export default class LineMapper {
  public static toDomains(waitingTimesEntity: WaitingTimeEntity[]): Line[] {
    if (!waitingTimesEntity || waitingTimesEntity.length == 0) {
      return [];
    }
    const dictLines: Map<string, Line> = new Map<string, Line>();

    waitingTimesEntity.forEach((entity) => {
      let line: Line = dictLines.get(entity.ligne.numLigne);
      if (!line) {
        line = {
          line: entity.ligne.numLigne,
          lineType: getLineType(entity.ligne.typeLigne),
          directions: { forward: undefined, backward: undefined },
        };
      }
      if (entity.sens == Direction.FORWARD) {
        line.directions.forward = manageDirections(
          line.directions.forward,
          entity,
        );
      } else {
        line.directions.backward = manageDirections(
          line.directions.backward,
          entity,
        );
      }
      dictLines.set(entity.ligne.numLigne, line);
    });
    return Array.from(dictLines.values()).sort((a, b) =>
      a.line < b.line ? -1 : 1,
    );
  }
}

function getLineType(typeLigne: number): LineType {
  switch (typeLigne) {
    case 1:
      return LineType.TRAM;
    case 2:
      return LineType.BUSWAY;
    case 3:
      return LineType.BUS;
    case 4:
      return LineType.NAVIBUS;
    default:
      return LineType.UNKNOWN;
  }
}

function manageDirections(
  actualDirections: DirectionValue[],
  entity: WaitingTimeEntity,
): DirectionValue[] {
  let newDirections: DirectionValue[] = actualDirections;
  if (!newDirections) {
    newDirections = [];
  }
  if (
    !newDirections.find((direction) => direction.terminus == entity.terminus)
  ) {
    newDirections.push({
      terminus: entity.terminus,
      stopId: entity.arret.codeArret,
      times: [],
    });
  }

  if (
    newDirections.find((direction) => direction.terminus == entity.terminus)
      .times.length < 4
  ) {
    newDirections
      .find((direction) => direction.terminus == entity.terminus)
      .times.push({ time: entity.temps });
  }

  return newDirections.sort((a, b) => (a.terminus < b.terminus ? -1 : 1));
}
