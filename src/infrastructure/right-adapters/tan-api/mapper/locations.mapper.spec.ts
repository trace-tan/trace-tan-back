import Location from '../../../../domain/models/location.model';
import { LocationEntity } from '../entity/location.entity';
import LocationMapper from './locations.mapper';

describe('LocationsMapper', () => {
  describe('toDomains', () => {
    it('should return empty array', () => {
      // GIVEN
      const locationEntitiesArray = [];
      // WHEN
      const resultLineArray = LocationMapper.toDomains(locationEntitiesArray);
      // THEN
      expect(resultLineArray.length).toBe(0);
    });

    it('should return a location with id and label and empty lines', () => {
      // GIVEN
      const locationEntitiesArray: LocationEntity[] = [
        { codeLieu: 'BSEJ', libelle: 'Beauséjour', ligne: [] },
      ];
      const expectedResult: Location[] = [
        {
          locationId: 'BSEJ',
          label: 'Beauséjour',
          lines: [],
        },
      ];
      // WHEN
      const resultLineArray = LocationMapper.toDomains(locationEntitiesArray);
      // THEN
      expect(resultLineArray.length).toBe(1);
      expect(resultLineArray).toStrictEqual(expectedResult);
    });

    it('should return a location with id label and lines', () => {
      // GIVEN
      const locationEntitiesArray: LocationEntity[] = [
        {
          codeLieu: 'BSEJ',
          libelle: 'Beauséjour',
          ligne: [{ numLigne: '1' }, { numLigne: '4' }, { numLigne: 'C3' }],
        },
      ];
      const expectedResult: Location[] = [
        {
          locationId: 'BSEJ',
          label: 'Beauséjour',
          lines: [{ line: '1' }, { line: '4' }, { line: 'C3' }],
        },
      ];
      // WHEN
      const resultLineArray = LocationMapper.toDomains(locationEntitiesArray);
      // THEN
      expect(resultLineArray.length).toBe(1);
      expect(resultLineArray).toStrictEqual(expectedResult);
    });

    it('should return multiples location with id label and lines', () => {
      // GIVEN
      const locationEntitiesArray: LocationEntity[] = [
        {
          codeLieu: 'BSEJ',
          libelle: 'Beauséjour',
          ligne: [{ numLigne: '1' }, { numLigne: '4' }, { numLigne: 'C3' }],
        },
        {
          codeLieu: 'COMM',
          libelle: 'Commerce',
          ligne: [
            { numLigne: '1' },
            { numLigne: '2' },
            { numLigne: '3' },
            { numLigne: '54' },
          ],
        },
        {
          codeLieu: 'MAI8',
          libelle: '8 Mai',
          ligne: [{ numLigne: 'C1' }, { numLigne: '54' }],
        },
      ];
      const expectedResult: Location[] = [
        {
          locationId: 'BSEJ',
          label: 'Beauséjour',
          lines: [{ line: '1' }, { line: '4' }, { line: 'C3' }],
        },
        {
          locationId: 'COMM',
          label: 'Commerce',
          lines: [{ line: '1' }, { line: '2' }, { line: '3' }, { line: '54' }],
        },
        {
          locationId: 'MAI8',
          label: '8 Mai',
          lines: [{ line: 'C1' }, { line: '54' }],
        },
      ];
      // WHEN
      const resultLineArray = LocationMapper.toDomains(locationEntitiesArray);
      // THEN
      expect(resultLineArray.length).toBe(3);
      expect(resultLineArray).toStrictEqual(expectedResult);
    });
  });
});
