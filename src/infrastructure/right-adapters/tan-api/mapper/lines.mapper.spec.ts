import { LineType } from '../../../../domain/models/line-type.enum';
import LineMapper from './lines.mapper';

describe('LinesMapper', () => {
  describe('toDomains', () => {
    it('should return empty array', () => {
      // GIVEN
      const waitingTimeEntityArray = [];

      // WHEN
      const resultLineArray = LineMapper.toDomains(waitingTimeEntityArray);
      // THEN
      expect(resultLineArray.length).toBe(0);
    });

    it('should return an array whith one line and one time', () => {
      // GIVEN
      const waitingTimeEntityArray = [
        {
          sens: 2,
          terminus: 'Bd de Doulon',
          temps: '2 mn',
          ligne: { numLigne: 'C3', typeLigne: 3 },
          arret: { codeArret: 'COME3' },
        },
      ];
      const expectedResult = [
        {
          line: 'C3',
          lineType: LineType.BUS,
          directions: {
            backward: [
              {
                terminus: 'Bd de Doulon',
                stopId: 'COME3',
                times: [{ time: '2 mn' }],
              },
            ],
          },
        },
      ];

      // WHEN
      const resultLineArray = LineMapper.toDomains(waitingTimeEntityArray);
      // THEN
      expect(resultLineArray.length).toBe(1);
      expect(resultLineArray).toEqual(expectedResult);
    });

    it('should return an array whith one line and multiple time', () => {
      // GIVEN
      const waitingTimeEntityArray = [
        {
          sens: 2,
          terminus: 'Bd de Doulon',
          temps: '2 mn',
          ligne: { numLigne: 'C3', typeLigne: 3 },
          arret: { codeArret: 'COME3' },
        },
        {
          sens: 2,
          terminus: 'Bd de Doulon',
          temps: '6 mn',
          ligne: { numLigne: 'C3', typeLigne: 3 },
          arret: { codeArret: 'COME3' },
        },
        {
          sens: 2,
          terminus: 'Bd de Doulon',
          temps: '14 mn',
          ligne: { numLigne: 'C3', typeLigne: 3 },
          arret: { codeArret: 'COME3' },
        },
      ];
      const expectedResult = [
        {
          line: 'C3',
          lineType: LineType.BUS,
          directions: {
            backward: [
              {
                terminus: 'Bd de Doulon',
                stopId: 'COME3',
                times: [{ time: '2 mn' }, { time: '6 mn' }, { time: '14 mn' }],
              },
            ],
          },
        },
      ];

      // WHEN
      const resultLineArray = LineMapper.toDomains(waitingTimeEntityArray);
      // THEN
      expect(resultLineArray.length).toBe(1);
      expect(resultLineArray).toEqual(expectedResult);
    });

    it('should return an array whith one line and multiple terminus sorted', () => {
      // GIVEN
      const waitingTimeEntityArray = [
        {
          sens: 2,
          terminus: 'Bd de Doulon',
          temps: '2 mn',
          ligne: { numLigne: 'C3', typeLigne: 3 },
          arret: { codeArret: 'COME3' },
        },
        {
          sens: 2,
          terminus: 'Bd de Doulon',
          temps: '6 mn',
          ligne: { numLigne: 'C3', typeLigne: 3 },
          arret: { codeArret: 'COME3' },
        },
        {
          sens: 2,
          terminus: 'Arret Michel',
          temps: '3 mn',
          ligne: { numLigne: 'C3', typeLigne: 3 },
          arret: { codeArret: 'COME3' },
        },
      ];
      const expectedResult = [
        {
          line: 'C3',
          lineType: LineType.BUS,
          directions: {
            backward: [
              {
                terminus: 'Arret Michel',
                stopId: 'COME3',
                times: [{ time: '3 mn' }],
              },
              {
                terminus: 'Bd de Doulon',
                stopId: 'COME3',
                times: [{ time: '2 mn' }, { time: '6 mn' }],
              },
            ],
          },
        },
      ];

      // WHEN
      const resultLineArray = LineMapper.toDomains(waitingTimeEntityArray);
      // THEN
      expect(resultLineArray.length).toBe(1);
      expect(resultLineArray).toEqual(expectedResult);
    });

    it('should return an array whith one line and multiple directions', () => {
      // GIVEN
      const waitingTimeEntityArray = [
        {
          sens: 2,
          terminus: 'Bd de Doulon',
          temps: '2 mn',
          ligne: { numLigne: 'C3', typeLigne: 3 },
          arret: { codeArret: 'COME3' },
        },
        {
          sens: 2,
          terminus: 'Bd de Doulon',
          temps: '6 mn',
          ligne: { numLigne: 'C3', typeLigne: 3 },
          arret: { codeArret: 'COME3' },
        },
        {
          sens: 1,
          terminus: 'Arret Michel',
          temps: '3 mn',
          ligne: { numLigne: 'C3', typeLigne: 3 },
          arret: { codeArret: 'COME3' },
        },
      ];
      const expectedResult = [
        {
          line: 'C3',
          lineType: LineType.BUS,
          directions: {
            backward: [
              {
                terminus: 'Bd de Doulon',
                stopId: 'COME3',
                times: [{ time: '2 mn' }, { time: '6 mn' }],
              },
            ],
            forward: [
              {
                terminus: 'Arret Michel',
                stopId: 'COME3',
                times: [{ time: '3 mn' }],
              },
            ],
          },
        },
      ];

      // WHEN
      const resultLineArray = LineMapper.toDomains(waitingTimeEntityArray);
      // THEN
      expect(resultLineArray.length).toBe(1);
      expect(resultLineArray).toEqual(expectedResult);
    });

    it('should return an array whith multiple lines sorted', () => {
      // GIVEN
      const waitingTimeEntityArray = [
        {
          sens: 1,
          terminus: 'Bd de Doulon',
          temps: '2 mn',
          ligne: { numLigne: 'C3', typeLigne: 3 },
          arret: { codeArret: 'COME3' },
        },
        {
          sens: 2,
          terminus: 'Commerce',
          temps: '6 mn',
          ligne: { numLigne: '1', typeLigne: 1 },
          arret: { codeArret: 'COME1' },
        },
        {
          sens: 1,
          terminus: 'Arret Michel',
          temps: '3 mn',
          ligne: { numLigne: 'C1', typeLigne: 3 },
          arret: { codeArret: 'COME2' },
        },
      ];
      const expectedResult = [
        {
          line: '1',
          lineType: LineType.TRAM,
          directions: {
            backward: [
              {
                terminus: 'Commerce',
                stopId: 'COME1',
                times: [{ time: '6 mn' }],
              },
            ],
          },
        },
        {
          line: 'C1',
          lineType: LineType.BUS,
          directions: {
            forward: [
              {
                terminus: 'Arret Michel',
                stopId: 'COME2',
                times: [{ time: '3 mn' }],
              },
            ],
          },
        },
        {
          line: 'C3',
          lineType: LineType.BUS,
          directions: {
            forward: [
              {
                terminus: 'Bd de Doulon',
                stopId: 'COME3',
                times: [{ time: '2 mn' }],
              },
            ],
          },
        },
      ];

      // WHEN
      const resultLineArray = LineMapper.toDomains(waitingTimeEntityArray);
      // THEN
      expect(resultLineArray.length).toBe(3);
      expect(resultLineArray).toEqual(expectedResult);
    });

    it('should return only 4 time (maximum)', () => {
      // GIVEN
      const waitingTimeEntityArray = [
        {
          sens: 1,
          terminus: 'Bd de Doulon',
          temps: '2 mn',
          ligne: { numLigne: 'C3', typeLigne: 3 },
          arret: { codeArret: 'COME3' },
        },
        {
          sens: 1,
          terminus: 'Bd de Doulon',
          temps: '3 mn',
          ligne: { numLigne: 'C3', typeLigne: 3 },
          arret: { codeArret: 'COME3' },
        },
        {
          sens: 1,
          terminus: 'Bd de Doulon',
          temps: '4 mn',
          ligne: { numLigne: 'C3', typeLigne: 3 },
          arret: { codeArret: 'COME3' },
        },
        {
          sens: 1,
          terminus: 'Bd de Doulon',
          temps: '5 mn',
          ligne: { numLigne: 'C3', typeLigne: 3 },
          arret: { codeArret: 'COME3' },
        },
        {
          sens: 1,
          terminus: 'Bd de Doulon',
          temps: '6 mn',
          ligne: { numLigne: 'C3', typeLigne: 3 },
          arret: { codeArret: 'COME3' },
        },
      ];
      const expectedResult = [
        {
          line: 'C3',
          lineType: LineType.BUS,
          directions: {
            forward: [
              {
                terminus: 'Bd de Doulon',
                stopId: 'COME3',
                times: [
                  { time: '2 mn' },
                  { time: '3 mn' },
                  { time: '4 mn' },
                  { time: '5 mn' },
                ],
              },
            ],
          },
        },
      ];

      // WHEN
      const resultLineArray = LineMapper.toDomains(waitingTimeEntityArray);
      // THEN
      expect(resultLineArray.length).toBe(1);
      expect(resultLineArray).toEqual(expectedResult);
    });
  });
});
