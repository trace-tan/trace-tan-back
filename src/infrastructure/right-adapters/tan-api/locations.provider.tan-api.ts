import { HttpService, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import Location from '../../../domain/models/location.model';
import { LocationsProvider } from '../../../domain/ports/locations.provider';
import LocationMapper from './mapper/locations.mapper';

@Injectable()
export class LocationsProviderTanApi implements LocationsProvider {
  constructor(
    private readonly httpService: HttpService,
    private readonly configService: ConfigService,
  ) {}
  getAll(): Promise<Location[]> {
    const url = this.configService.get('TAN_API_URL');
    return this.httpService
      .get(`${url}arrets.json`)
      .toPromise()
      .then((value) => {
        return LocationMapper.toDomains(value.data);
      });
  }
}
