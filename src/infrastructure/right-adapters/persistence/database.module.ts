import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import Delay from './entity/delay.entity';
import LineColor from './entity/line-color.entity';
import { DelayRepositoryPostgres } from './repository/delay.repository.postgres';
import { LinesColorRepositoryPostgres } from './repository/lines-color.repository.postgres';

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        type: 'postgres',
        url: configService.get('DATABASE_URL'),
        entities: [Delay, LineColor],
        synchronize: true,
      }),
    }),
    TypeOrmModule.forFeature([
      DelayRepositoryPostgres,
      LinesColorRepositoryPostgres,
    ]),
  ],
  exports: [TypeOrmModule],
})
export class DatabaseModule {}
