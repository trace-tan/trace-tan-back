import { EntityRepository, Repository } from 'typeorm';
import LineColorDto from '../../../../domain/models/line-color.dto';
import { LinesColorRepository } from '../../../../domain/ports/lines-color.repository';
import LineColor from '../entity/line-color.entity';

@EntityRepository(LineColor)
export class LinesColorRepositoryPostgres
  extends Repository<LineColor>
  implements LinesColorRepository {
  async findAll(): Promise<LineColorDto[]> {
    return await this.find();
  }
}
