import { DelayRepository } from '../../../../domain/ports/delay.repository';
import Delay from '../entity/delay.entity';
import { EntityRepository, Repository } from 'typeorm';
import NotifyDelayDto from '../../../../domain/models/notify-delay.dto';

@EntityRepository(Delay)
export class DelayRepositoryPostgres
  extends Repository<Delay>
  implements DelayRepository {
  async notifyDelay(notifyDelayDto: NotifyDelayDto): Promise<NotifyDelayDto> {
    const newDelay = this.create(notifyDelayDto);
    await this.save(newDelay);
    return newDelay;
  }
}
