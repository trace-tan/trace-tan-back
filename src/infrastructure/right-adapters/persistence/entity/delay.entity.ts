import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
class Delay {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column()
  public stopId: string;

  @Column()
  public terminusDirection: string;

  @Column()
  public departureTime: Date;

  @Column()
  public delay: number;

  @Column()
  public line: string;
}

export default Delay;
