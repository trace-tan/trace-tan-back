import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
class LineColor {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column()
  public line: string;

  @Column()
  public color: string;

  @Column()
  public textColor: string;
}

export default LineColor;
