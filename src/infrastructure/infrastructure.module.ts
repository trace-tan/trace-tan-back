import { CacheInterceptor, CacheModule, Module } from '@nestjs/common';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { ApplicationModule } from '../application/application.module';
import { DelayController } from './left-adapters/controllers/delay.controller';
import { LinesColorController } from './left-adapters/controllers/lines-color.controller';
import { LocationsController } from './left-adapters/controllers/locations.controller';
import { WaitingTimeController } from './left-adapters/controllers/waiting-time.controller';

@Module({
  controllers: [
    WaitingTimeController,
    LocationsController,
    DelayController,
    LinesColorController,
  ],
  imports: [ApplicationModule, CacheModule.register()],
  providers: [
    {
      provide: APP_INTERCEPTOR,
      useClass: CacheInterceptor,
    },
  ],
})
export class InfrastructureModule {}
