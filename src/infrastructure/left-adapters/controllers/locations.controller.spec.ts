import { Test, TestingModule } from '@nestjs/testing';
import { ListLocationsUseCase } from '../../../application/use-cases/list-locations.usecase';
import locationModel from '../../../domain/models/location.model';
import { LocationsProvider } from '../../../domain/ports/locations.provider';
import { LocationsController } from './locations.controller';

class LocationsProviderInMemory implements LocationsProvider {
  getAll(): Promise<locationModel[]> {
    return new Promise((resolve) => resolve([]));
  }
}

describe('LocationsController', () => {
  let controller: LocationsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [LocationsController],
      providers: [
        ListLocationsUseCase,
        {
          provide: 'LocationsProvider',
          useClass: LocationsProviderInMemory,
        },
      ],
    }).compile();

    controller = module.get<LocationsController>(LocationsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
