import { Test, TestingModule } from '@nestjs/testing';
import { NotifyDelayUseCase } from '../../../application/use-cases/notify-delay';
import delayModel from '../../../domain/models/notify-delay.dto';
import { DelayRepository } from '../../../domain/ports/delay.repository';
import { DelayController } from './delay.controller';

class DelayRepositoryInMemory implements DelayRepository {
  notifyDelay(notifyDelayDto: delayModel): Promise<delayModel> {
    return new Promise((resolve) => resolve(notifyDelayDto));
  }
}

describe('DelayController', () => {
  let controller: DelayController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [DelayController],
      providers: [
        NotifyDelayUseCase,
        {
          provide: 'DelayRepository',
          useClass: DelayRepositoryInMemory,
        },
      ],
    }).compile();

    controller = module.get<DelayController>(DelayController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
