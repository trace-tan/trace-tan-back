import { Body, Controller, Post } from '@nestjs/common';
import { NotifyDelayUseCase } from '../../../application/use-cases/notify-delay';
import NotifyDelayDto from '../../../domain/models/notify-delay.dto';

@Controller('delay')
export class DelayController {
  constructor(private notifyDelayUseCase: NotifyDelayUseCase) {}

  @Post()
  async notifyDelay(@Body() notifyDelayDto: NotifyDelayDto) {
    return await this.notifyDelayUseCase.handler(notifyDelayDto);
  }
}
