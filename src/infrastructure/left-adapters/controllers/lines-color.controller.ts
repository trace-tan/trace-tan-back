import { CacheTTL, Controller, Get } from '@nestjs/common';
import { GetLinesColorUseCase } from '../../../application/use-cases/get-lines-color';

@Controller('lines-color')
export class LinesColorController {
  constructor(private getLinesColorUseCase: GetLinesColorUseCase) {}

  @CacheTTL(43200) // 12 hours
  @Get()
  async getLinesColor() {
    return await this.getLinesColorUseCase.handler();
  }
}
