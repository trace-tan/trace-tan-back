import { Test, TestingModule } from '@nestjs/testing';
import { GetWaitingTimesOfLocationUseCase } from '../../../application/use-cases/get-waiting-times-of-location.usecase';
import Line from '../../../domain/models/line.model';
import { WaitingTimesProvider } from '../../../domain/ports/waiting-times.provider';
import { WaitingTimeController } from './waiting-time.controller';

class WaitingTimesProviderInMemory implements WaitingTimesProvider {
  getByLocationId(locationId: string): Promise<Line[]> {
    return new Promise((resolve) => resolve([]));
  }
}

describe('WaitingTimeController', () => {
  let controller: WaitingTimeController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [WaitingTimeController],
      providers: [
        GetWaitingTimesOfLocationUseCase,
        {
          provide: 'WaitingTimesProvider',
          useClass: WaitingTimesProviderInMemory,
        },
      ],
    }).compile();

    controller = module.get<WaitingTimeController>(WaitingTimeController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
