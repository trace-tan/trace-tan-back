import { CacheTTL, Controller, Get, Param } from '@nestjs/common';
import { GetWaitingTimesOfLocationUseCase } from '../../../application/use-cases/get-waiting-times-of-location.usecase';
import Line from '../../../domain/models/line.model';

@Controller('waiting-time')
export class WaitingTimeController {
  constructor(
    private getWaitingTimeOfLocationUseCase: GetWaitingTimesOfLocationUseCase,
  ) {}

  @CacheTTL(10) // 10 seconds
  @Get(':locationId')
  public async getWaitingTimesOfLocation(
    @Param('locationId') locationId,
  ): Promise<Line[]> {
    return await this.getWaitingTimeOfLocationUseCase.handler(locationId);
  }
}
