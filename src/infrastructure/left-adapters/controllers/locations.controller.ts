import { CacheTTL, Controller, Get } from '@nestjs/common';
import { ListLocationsUseCase } from '../../../application/use-cases/list-locations.usecase';
import Location from '../../../domain/models/location.model';

@Controller('locations')
export class LocationsController {
  constructor(private listLocationsUseCase: ListLocationsUseCase) {}

  @CacheTTL(43200) // 12 hours
  @Get()
  public async getLocations(): Promise<Location[]> {
    return await this.listLocationsUseCase.handler();
  }
}
