import { Test, TestingModule } from '@nestjs/testing';
import { of } from 'rxjs';
import { GetLinesColorUseCase } from '../../../application/use-cases/get-lines-color';
import LineColorDto from '../../../domain/models/line-color.dto';
import { LinesColorRepository } from '../../../domain/ports/lines-color.repository';
import { LinesColorController } from './lines-color.controller';

class LinesColorRepositoryInMemory implements LinesColorRepository {
  findAll(): Promise<LineColorDto[]> {
    return of([]).toPromise();
  }
}

describe('LinesColorController', () => {
  let controller: LinesColorController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [LinesColorController],
      providers: [
        GetLinesColorUseCase,
        {
          provide: 'LinesColorRepository',
          useClass: LinesColorRepositoryInMemory,
        },
      ],
    }).compile();

    controller = module.get<LinesColorController>(LinesColorController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
