import { Test, TestingModule } from '@nestjs/testing';
import { HttpService, INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from '../../src/app.module';
import { of } from 'rxjs';

describe('WaitingTimeController (e2e)', () => {
  let app: INestApplication;
  const inMemoryHttpService = {
    get: () => {
      return of({
        data: [
          {
            sens: 1,
            terminus: 'Jamet',
            temps: '1 mn',
            ligne: { numLigne: '1', typeLigne: 1 },
            arret: { codeArret: 'COMB2' },
          },
          {
            sens: 1,
            terminus: 'François Mitterrand',
            temps: '2 mn',
            ligne: { numLigne: '1', typeLigne: 1 },
            arret: { codeArret: 'COMB2' },
          },
          {
            sens: 2,
            terminus: 'Ranzay',
            temps: '2 mn',
            ligne: { numLigne: '1', typeLigne: 1 },
            arret: { codeArret: 'COMC1' },
          },
          {
            sens: 1,
            terminus: 'Jamet',
            temps: '4 mn',
            ligne: { numLigne: '1', typeLigne: 1 },
            arret: { codeArret: 'COMB2' },
          },
          {
            sens: 2,
            terminus: 'Beaujoire',
            temps: '4 mn',
            ligne: { numLigne: '1', typeLigne: 1 },
            arret: { codeArret: 'COMC1' },
          },
          {
            sens: 1,
            terminus: 'François Mitterrand',
            temps: '5 mn',
            ligne: { numLigne: '1', typeLigne: 1 },
            arret: { codeArret: 'COMB2' },
          },
          {
            sens: 2,
            terminus: 'Ranzay',
            temps: '7 mn',
            ligne: { numLigne: '1', typeLigne: 1 },
            arret: { codeArret: 'COMC1' },
          },
          {
            sens: 2,
            terminus: 'Saupin',
            temps: '9 mn',
            ligne: { numLigne: '54', typeLigne: 3 },
            arret: { codeArret: 'COME3' },
          },
          {
            sens: 1,
            terminus: 'Jamet',
            temps: '9 mn',
            ligne: { numLigne: '1', typeLigne: 1 },
            arret: { codeArret: 'COMB2' },
          },
          {
            sens: 1,
            terminus: 'Marcel Paul',
            temps: '10 mn',
            ligne: { numLigne: '54', typeLigne: 3 },
            arret: { codeArret: 'COMF1' },
          },
          {
            sens: 1,
            terminus: 'François Mitterrand',
            temps: '10 mn',
            ligne: { numLigne: '1', typeLigne: 1 },
            arret: { codeArret: 'COMB2' },
          },
          {
            sens: 2,
            terminus: 'Beaujoire',
            temps: '10 mn',
            ligne: { numLigne: '1', typeLigne: 1 },
            arret: { codeArret: 'COMC1' },
          },
          {
            sens: 1,
            terminus: 'Jamet',
            temps: '12 mn',
            ligne: { numLigne: '1', typeLigne: 1 },
            arret: { codeArret: 'COMB2' },
          },
          {
            sens: 1,
            terminus: 'François Mitterrand',
            temps: '13 mn',
            ligne: { numLigne: '1', typeLigne: 1 },
            arret: { codeArret: 'COMB2' },
          },
          {
            sens: 2,
            terminus: 'Beaujoire',
            temps: '13 mn',
            ligne: { numLigne: '1', typeLigne: 1 },
            arret: { codeArret: 'COMC1' },
          },
          {
            sens: 2,
            terminus: 'Ranzay',
            temps: '13 mn',
            ligne: { numLigne: '1', typeLigne: 1 },
            arret: { codeArret: 'COMC1' },
          },
          {
            sens: 1,
            terminus: 'Marcel Paul',
            temps: '15 mn',
            ligne: { numLigne: '54', typeLigne: 3 },
            arret: { codeArret: 'COMF1' },
          },
          {
            sens: 2,
            terminus: 'Saupin',
            temps: '22 mn',
            ligne: { numLigne: '54', typeLigne: 3 },
            arret: { codeArret: 'COME3' },
          },
          {
            sens: 2,
            terminus: 'Beaujoire',
            temps: '22 mn',
            ligne: { numLigne: '1', typeLigne: 1 },
            arret: { codeArret: 'COMC1' },
          },
          {
            sens: 2,
            terminus: 'Ranzay',
            temps: '45 mn',
            ligne: { numLigne: '1', typeLigne: 1 },
            arret: { codeArret: 'COMC1' },
          },
        ],
      });
    },
  };

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    })
      .overrideProvider(HttpService)
      .useValue(inMemoryHttpService)
      .compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/waiting-time/<locationId> (GET)', () => {
    return request(app.getHttpServer())
      .get('/waiting-time/COMM')
      .expect(200)
      .expect(
        JSON.stringify([
          {
            line: '1',
            lineType: 'tramway',
            directions: {
              forward: [
                {
                  terminus: 'François Mitterrand',
                  stopId: 'COMB2',
                  times: [
                    { time: '2 mn' },
                    { time: '5 mn' },
                    { time: '10 mn' },
                    { time: '13 mn' },
                  ],
                },
                {
                  terminus: 'Jamet',
                  stopId: 'COMB2',
                  times: [
                    { time: '1 mn' },
                    { time: '4 mn' },
                    { time: '9 mn' },
                    { time: '12 mn' },
                  ],
                },
              ],
              backward: [
                {
                  terminus: 'Beaujoire',
                  stopId: 'COMC1',
                  times: [
                    { time: '4 mn' },
                    { time: '10 mn' },
                    { time: '13 mn' },
                    { time: '22 mn' },
                  ],
                },
                {
                  terminus: 'Ranzay',
                  stopId: 'COMC1',
                  times: [
                    { time: '2 mn' },
                    { time: '7 mn' },
                    { time: '13 mn' },
                    { time: '45 mn' },
                  ],
                },
              ],
            },
          },
          {
            line: '54',
            lineType: 'bus',
            directions: {
              forward: [
                {
                  terminus: 'Marcel Paul',
                  stopId: 'COMF1',
                  times: [{ time: '10 mn' }, { time: '15 mn' }],
                },
              ],
              backward: [
                {
                  terminus: 'Saupin',
                  stopId: 'COME3',
                  times: [{ time: '9 mn' }, { time: '22 mn' }],
                },
              ],
            },
          },
        ]),
      );
  });
});
