import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from '../../src/app.module';
import { of } from 'rxjs';
import { DelayRepository } from '../../src/domain/ports/delay.repository';

describe('DelayController (e2e)', () => {
  let app: INestApplication;
  const inMemoryRepository: DelayRepository = {
    notifyDelay: (notifyDelayDto) => {
      return of({ id: 1, ...notifyDelayDto }).toPromise();
    },
  };

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    })
      .overrideProvider('DelayRepository')
      .useValue(inMemoryRepository)
      .compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/delay (POST)', () => {
    return request(app.getHttpServer())
      .post('/delay')
      .send({
        delay: 2,
        departureTime: '2021-06-19 12:42:05',
        stopId: 'COMM',
        terminusDirection: 'Marcel Paul',
        line: '3',
      })
      .expect(201)
      .expect({
        id: 1,
        delay: 2,
        departureTime: '2021-06-19 12:42:05',
        stopId: 'COMM',
        terminusDirection: 'Marcel Paul',
        line: '3',
      });
  });
});
