import { Test, TestingModule } from '@nestjs/testing';
import { HttpService, INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from '../../src/app.module';
import { of } from 'rxjs';

describe('LocationsController (e2e)', () => {
  let app: INestApplication;
  const inMemoryHttpService = {
    get: () => {
      return of({
        data: [
          {
            codeLieu: 'OTAG',
            libelle: '50 Otages',
            ligne: [
              { numLigne: '2' },
              { numLigne: '2B' },
              { numLigne: 'C2' },
              { numLigne: '12' },
              { numLigne: '23' },
            ],
          },
          {
            codeLieu: 'MAI8',
            libelle: '8 Mai',
            ligne: [
              { numLigne: '2' },
              { numLigne: '3' },
              { numLigne: '97' },
              { numLigne: '98' },
            ],
          },
          {
            codeLieu: 'BSEJ',
            libelle: 'Beauséjour',
            ligne: [
              { numLigne: '3' },
              { numLigne: 'C20' },
              { numLigne: '12' },
              { numLigne: '59' },
              { numLigne: '79' },
              { numLigne: '89' },
              { numLigne: '96' },
            ],
          },
        ],
      });
    },
  };

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    })
      .overrideProvider(HttpService)
      .useValue(inMemoryHttpService)
      .compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/locations (GET)', () => {
    return request(app.getHttpServer())
      .get('/locations')
      .expect(200)
      .expect([
        {
          locationId: 'OTAG',
          label: '50 Otages',
          lines: [
            { line: '2' },
            { line: '2B' },
            { line: 'C2' },
            { line: '12' },
            { line: '23' },
          ],
        },
        {
          locationId: 'MAI8',
          label: '8 Mai',
          lines: [{ line: '2' }, { line: '3' }, { line: '97' }, { line: '98' }],
        },
        {
          locationId: 'BSEJ',
          label: 'Beauséjour',
          lines: [
            { line: '3' },
            { line: 'C20' },
            { line: '12' },
            { line: '59' },
            { line: '79' },
            { line: '89' },
            { line: '96' },
          ],
        },
      ]);
  });
});
