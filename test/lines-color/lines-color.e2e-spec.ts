import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from '../../src/app.module';
import { of } from 'rxjs';
import { LinesColorRepository } from '../../src/domain/ports/lines-color.repository';

describe('LinesColorController (e2e)', () => {
  let app: INestApplication;
  const inMemoryRepository: LinesColorRepository = {
    findAll: () => {
      return of([
        {
          line: '1',
          color: '#007A45',
          textColor: '#FFFFFF',
        },
        {
          line: '2',
          color: '#E53138',
          textColor: '#FFFFFF',
        },
        {
          line: '3',
          color: '#0079BC',
          textColor: '#FFFFFF',
        },
      ]).toPromise();
    },
  };

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    })
      .overrideProvider('LinesColorRepository')
      .useValue(inMemoryRepository)
      .compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/lines-color (GET)', () => {
    return request(app.getHttpServer())
      .get('/lines-color')
      .expect(200)
      .expect([
        {
          line: '1',
          color: '#007A45',
          textColor: '#FFFFFF',
        },
        {
          line: '2',
          color: '#E53138',
          textColor: '#FFFFFF',
        },
        {
          line: '3',
          color: '#0079BC',
          textColor: '#FFFFFF',
        },
      ]);
  });
});
